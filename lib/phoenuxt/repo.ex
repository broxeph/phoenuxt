defmodule Phoenuxt.Repo do
  use Ecto.Repo,
    otp_app: :phoenuxt,
    adapter: Ecto.Adapters.Postgres
end
