# PhoeNuxt

In which Alex learns Phoenix, Vue, Nuxt.js, GraphQL, ECS, and CDK!

Not sure what to do with it, though. Surely there's something useful this stack can do...

## Run the Trap

1. Install dependencies: `mix deps.get`
2. Create and migrate database: `mix ecto.setup`
3. Start Phoenix endpoint: `mix phx.server`

GraphiQL: http://localhost:4000/graphiql

LiveDashboard: http://localhost:4000/dashboard

## Stack

### Frontend

- Vue
- Apollo
- Bootstrap  # TODO
- Nuxt.js  # TODO

### Backend

- Elixir
- Phoenix
- GraphQL
- Absinthe
- Postgres  # TODO
- Docker  # TODO

### Infra

- CDK  # TODO
- ECS  # TODO
- RDS  # TODO
- CloudFront  # TODO

## To Do

- [Dockerize](https://pspdfkit.com/blog/2018/how-to-run-your-phoenix-application-with-docker/)
- Replace fake db with Postgres
- Separate frontend, set up Vue router (Nuxt?)
- Add Bootstrap ([BootstrapVue?](https://bootstrap-vue.org/docs))
- GitLab CI
- [Productionize](https://hexdocs.pm/phoenix/deployment.html)
- Create CDK templates
- GitLab CD to AWS
